import React, { Component } from "react";
import Shape from "./Shape"
import "./App.css"
import { EventEmitter } from "events";
import { Color } from "three";

const colors = [
  "#c0392b",
  "#2980b9",
  "#9b59b6",
  "#2c3e50",
  "#34495e"
]
class App extends Component {
  state = {
    xspeed : 0,
    yspeed: 0,
    isrendered:false,
    hexcolor : {
      r: 0,
      g: 0,
      b: 0
    },
    hexval : null
  }

  handleXChange = (event) => { 
    this.setState({xspeed: event.target.value})

  }

  handleYChange = (event) => {
    this.setState({yspeed: event.target.value})
  }

  onRenderbuttonclick = () => {
    this.setState({isrendered : !this.state.isrendered})
  }

  setHexVal = (event) => {
    this.setState({hexval : parseInt(event.target.value)})
  }

  render() {
    const {xspeed, yspeed,isrendered, hexval} = this.state
    return (
      <div>
        <p>X speed</p>
        <input type="number" onChange={this.handleXChange} placeholder="Set X speed"></input>
        <p>Y speed</p>
        <input type="number" onChange={this.handleYChange} placeholder="set Y speed"></input>   
        <div>
          <div> Color Pallet</div>  
          <div> 
            {
              colors.map( (color, i=0) => 
                <button 
                key = {i++}
                className="color-button" 
                style={ {backgroundColor : color } } 
                onClick={this.setHexVal} value={color.replace("#","0x")}/>

  
              )
            }
          </div>
        </div>

        <button onClick={this.onRenderbuttonclick}>Render Model</button>
        {(isrendered) ? <Shape 
            xspeed = {xspeed}
            yspeed = {yspeed}
            hexcolor = {hexval}
          />
          :
          <p>Not rendered</p>
        }  
      </div>
    );
  }
}
export default App;